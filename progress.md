# Week 7 - PCB
Made a pcb so it's easier to replicate for others, images can be found in the PCB folder, together with the Gerber files, project is open on [easyEDA](https://easyeda.com/jerzeek/air-quality-pcb)
- added BOM
- cleaned code
- made nicer graph
- added data smoothing

<img src="PCB/Front-pcb-3D-models.png" width="200">

# Week6 - Case
I did some simple 3D modelling  and that resulted in a simple ABS case that holds the sensor and makes it easy to use anywhere. files are in the 3D folder

<img src="img/sensor.gif" width="200">




# Week5 - Back on Track
It has been a while, like a long long while. Sorry 😬 But even after this time, this project is not dead! I still think that this project is highly relevant and can reallt help others to stay save. 
- The BM*E*280 sensor finally arrived =) was easy to swap out
- Added some neopixels as indicatorsp
- started to move from breadboad more to pcb
- started modelling a simple case
- code is now totally messed up, it has bodge's on bodge's, would require a rewrite at some point

<img src="img/pcb.jpg" width="200">




# Week4 - Adding the wrong sensor
Added a BMP280 sensor, turns out there is a difference between a BM*P*280 sensor and a BM*E*280 sensor, i bought the P with also gives air pressure (totally useless in this case) and not the humidity. but it can still calibrate for the temperature offset. added some code so you can simply save the measured data as an .CSV file. Also started adding some filtering and smoothing, it's still quite rough but it gives an better indication of the measurements over time.

<img src="img/connection setup.png" width="200">

# Week3 - Changing Screen
Graph was overlaying with the text to made it a bit smaller (100px wide graph)
Cleaned the code a bit, made preperations for BME280 sensor
Thinking of maybe switching to a 0.91" screen (128x32 px) since this would keep the footprint of the device small.

# Week2 - Connecting css811 and oled
Made a breadboard prototype to test the sensor and display, got both working 😁
started with a simple program (oled.py) to show the measurements from the sensor on the display. Later added a graph to show the history of measurements. I made the graph so it autoscales on the maximum value of the last 128 measurements.
to do:
- make the graph look nice, and the numbers.
- store the different measurements over time on the system
- make an animation for the warm up time
- add temperature sensor

![alt text](img/test_setup.gif "test setup")

# Week1 - Getting to know MicroPython
For this project I wanted to learn something new: MircoPython!
It has been around for a couple of years now, started as a kickstarter project and basically is python for microcontrollers
instead of programming something in the Arduino IDE you can write python code and the microcontroller then executes the file once it boots up.

- I learned most about micropython in this [video](https://www.youtube.com/watch?v=m1miwCJtxeM)
- I flashed an esp32 and esp8266 according to this [video tutorial](https://www.youtube.com/watch?v=w15-EQASP_Y)
- and found great MicroPython resources in this [repo](https://github.com/mcauser/awesome-micropython)
