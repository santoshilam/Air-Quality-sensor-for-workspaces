"""Simple Fume sensor with the CCS811 + BME280 sensor, visualized with a 0.91 Oled screen and 4 Neopixels""""

from machine import Pin, I2C
import time
import CCS811, BME280
import ssd1306
from neopixel import NeoPixel

# i2c bus with scl = D1 (GPIO5) and sda= D2 (GPIO4)
i2c = I2C(scl=Pin(5), sda=Pin(4))
# CCS811 sensor  = s
s = CCS811.CCS811(i2c)
# BME280 sensor = b (including Temp, Hum, Pressure(?) )
b = BME280.BME280(i2c=i2c)
# Oled screen at 0x3c on i2x bus (64x128 pixels) 
d = ssd1306.SSD1306_I2C(128,64,i2c,0x3c)
# Neopixel output on D4 (GPIO0) with 4 lights
pin = Pin(0, Pin.OUT)
np = NeoPixel(pin, 4)

TVOC_values = [0,1]
avg_TVOC = [0,0,0]
long_average = 0




def main():
    n = 0
    time.sleep(1)
    while True:
        if s.data_ready() and n < 3:
            n+=1
            #print('eCO2: %d ppm, TVOC: %d ppb' % (s.eCO2, s.tVOC), b.values)
            avg_TVOC.insert(0,s.tVOC)
            if len(avg_TVOC) > 3:
                avg_TVOC.pop(3)
            r = b.read_compensated_data()
            global t
            t = r[0]/100
            p = r[1]/25600
            h = r[2]/1024
            x,y = s.get_baseline()
            s.put_envdata(humidity=h,temp=t)
        if n == 3:
            avg = (avg_TVOC[0]+avg_TVOC[1]+avg_TVOC[2])/3
            TVOC_values.insert(0,avg)
            if len(TVOC_values) > 100:
                TVOC_values.pop(100)
            print(avg)
            update_screen()
            n=0

############ showing information on the screen ############################
def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin
    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)
    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

############ showing information on the screen ############################
def update_screen():
    d.fill(0)
    upper = max(TVOC_values)
    minimal = min(TVOC_values)
    for i in range(len(TVOC_values)):
        #average = TVOC_values[i]
        average = (TVOC_values[i]+TVOC_values[i-1]+TVOC_values[i-2])/3 #a bit of smoothing
        j = int(translate(average, 0,upper,64,0))
        for x in range(64, j, -1):
            d.pixel(i, x, 1)
    long_average = sum(TVOC_values)/len(TVOC_values)
    d.text(str(int(long_average)),100,20)
    d.text(str(int(upper)),100,0)
    d.text(str(int(minimal)),100,54)
    d.text(str(int(t)),100,36)
    d.show()
    #print(average)
    #print(TVOC_values)
    update_lights(long_average)

############ set the lights how you want ############################
def update_lights(long_average):
    #print(long_average)
    if long_average < 150:
        np[0] = (255, 0, 0) # set the first pixel to white
        np[1] = (255, 0, 0) # set the first pixel to white
        np[2] = (255, 0, 0) # set the first pixel to white
        np[3] = (255, 0, 0) # set the first pixel to white
        np.write()
    elif long_average <200:
        np[0] = (180,80,0) # set the first pixel to white
        np[1] = (180,80,0) # set the first pixel to white
        np[2] = (180,80,0) # set the first pixel to white
        np[3] = (180,80,0) # set the first pixel to white
        np.write()
    else:
        np[0] = (0, 255, 0) # set the first pixel to white
        np[1] = (0, 255, 0) # set the first pixel to white
        np[2] = (0, 255, 0) # set the first pixel to white
        np[3] = (0, 255, 0) # set the first pixel to white
        np.write()
main()

############ uncomment this if you want to store values in a .csv file format ############################
#def store_value():
    #f = open('data.csv', 'a')
    #f.write(str(s.eCO2)+','+str(s.tVOC)+','+str(t)+','+str(p)+'\n')
    #f.close()